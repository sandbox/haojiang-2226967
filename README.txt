
Xunsearch
-----------
This module provides an implementation of the Search API.

CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
[English]
This module handles integration with XunSearch
an excellent fulltext search engine
expressly for Chinese and other Asian languages like Korean and Japanese 
and it support English well too.
Xunsearch is very high performance, GPL , very easy to install.
Xunsearch is written in C/C++ , 
use xapian-core , scws to handle Chinese segmentation scheme.
Install Xunsearch: http://www.xunsearch.com/doc/php/guide/start.installation
Install TestModule: http://www.trackself.com/archives/2539.html

[Chinese]
http://www.trackself.com/archives/2539.html

REQUIREMENTS
------------
This module requires the following modules:
 * Search_api (https://drupal.org/project/search_api)
 * Facetapi (https://drupal.org/project/facetapi)
 
INSTALLATION
------------
 * Detail installation https://drupal.org/node/2234637
 * Install Xunsearch: http://www.xunsearch.com/doc/php/guide/start.installation
   Xunsearch document:http://www.xunsearch.com/doc/php/api/index

    wget http://www.xunsearch.com/download/xunsearch-full-latest.tar.bz2
    tar -xjf xunsearch-full-latest.tar.bz2
    cd xunsearch-full-1.3.0/
    sh setup.sh
 * Start up Xunsearch:
    cd $prefix ; bin/xs-ctl.sh restart      
    (replace $prefix according to your own environment,
    default should be /usr/local/xunsearch)
 * Config ini file
    cp $prefix/sdk/php/app/demo.ini $prefix/sdk/php/app/d7.ini 
    vi d7.ini
    change the ini file to fit with drupal

 * Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.
 
 * Currently, this module support node and commerce product very well.

CONFIGURATION
-------------
Follow the configuration https://drupal.org/project/search_api
Currently, this module support node and commerce product very well.

TROUBLESHOOTING
---------------
 * Make sure you have a file call *.ini
   in xunsearch $prefix/sdk/php/app/ folder
 * Make sure your xunsearch install in a correct way
 
MAINTAINERS
-----------
Current maintainers:
 * Haojiang - https://drupal.org/user/250888
 * bloomingwhs - https://drupal.org/user/1409950
 * mediaventure - https://drupal.org/user/914736
This project has been sponsored by:
 * DRUPAAL ANYWHERE
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. For more information: 
  Visit http://www.trackself.com/archives/2539.html .
