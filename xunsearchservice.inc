<?php
/**
 * @file
 * Provides a Xunsearch-based service class for the Search API.
 * Update version.
 */

/**
 * XunSearchService class.
 */
class XunSearchService extends SearchApiAbstractService {
  /**
   * Implements SearchApiAlterCallbackInterface::configurationForm().
   */
  public function configurationForm(array $form, array &$form_state) {
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Xunsearch path'),
      '#description' => t(
          'The path after you install xunsearch server ,default is :/usr/local/xunsearch/sdk/php/lib/XS.php'),
      '#default_value' => "/usr/local/xunsearch/sdk/php/lib/XS.php",
      '#required' => TRUE,
    );
    $form['ininame'] = array(
      '#type' => 'textfield',
      '#title' => t('ini name'),
      '#description' => t(
          'ini name , you could find it on /usr/local/xunsearch/sdk/php/app/*'),
      '#default_value' => "d7",
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * SupportsFeature().
   */
  public function supportsFeature($feature) {
    $supported = drupal_map_assoc(
        array(
          'search_api_autocomplete',
          'search_api_facets',
          'search_api_service_extra',
          'search_api_data_type_xs_string',
          'search_api_data_type_xs_numeric',
          'search_api_data_type_xs_date',
          'search_api_data_type_xs_id',
          'search_api_data_type_xs_title',
          'search_api_data_type_xs_body',
        ));
    return isset($supported[$feature]);
  }

  /**
   * Convert a config type to xunsearch suport index type.
   */
  protected function getxstype($type = "string") {
    if (substr($type, 0, 3) == "xs_") {
      $type_exp = explode("_", $type);
      $xstype = $type_exp[1];
    }
    else {
      $xstype = $type;
    }
    return $xstype;
  }

  /**
   * Return wheather the config type is a xunsearch type.
   */
  protected function isxstype($type = "string") {
    return substr($type, 0, 3) == "xs_";
  }

  /**
   * Return weather the type is xunsearch id type.
   */
  protected function isxsidtype($type = "string") {
    return $type == "xs_id";
  }

  /**
   * Overrides SearchApiAbstractService::fieldsUpdated().
   *
   * Internally, this is also used by addIndex().
   */
  public function fieldsUpdated(SearchApiIndex $index) {
    try {

      $fields = $index->getFields();

      $inistr = "[id]<br/>type=id<br/>";
      foreach ($fields as $name => $field) {
        $type = isset($field['real_type']) ? $field['real_type'] : $field['type'];
        $xstype = $this->getxstype($type);
        if ($this->isxsidtype($type)) {
          $this->options["xs_index_id"] = $name;
          $inistr .= "[" . $name . "]<br/>";
          $inistr .= "type = string<br/>";
          $inistr .= "index = self<br/>";
        }
        elseif ($xstype == "string" ) {
          $inistr .= "[" . $name . "]<br/>";
          $inistr .= "type = string<br/>";
          $inistr .= "index = self<br/>";
        }
        elseif ($xstype == "text" ) {
          $inistr .= "[" . $name . "]<br/>";
          $inistr .= "type = string<br/>";
          $inistr .= "index = both<br/>";
        }
        elseif (!$this->isxstype($type)) {
          $inistr .= "[" . $name . "]<br/>";
          $inistr .= "type = string<br/>";
          $inistr .= "index = self<br/>";
        }
        else {
          $inistr .= "[" . $name . "]<br/>";
          $inistr .= "type = " . $xstype . "<br/>";
        }
      }
      variable_set("xunsearch_ini_str_" . $index->server()->machine_name,
          $inistr);
      xunsearch_cleanall($this->options['path'], $this->options['ininame']);
      $this->server->save();
      return TRUE;
    }
    catch (Exception $e) {
      throw new SearchApiException($e->getMessage());
    }
  }
  /**
   * IndexItems().
   */
  public function indexItems(SearchApiIndex $index, array $items) {
    $success = array();

    foreach ($items as $id => $item) {
      $this
          ->indexItem($index, $item, $this->options['path'],
              $this->options['ininame']);
      $success[] = $id;
    }
    $items = NULL;
    $index = NULL;
    return $success;
  }

  /**
   * Index one node item on xunsearch.
   */
  protected function indexItem(SearchApiIndex $sindex, $item,
      $path = '/usr/local/xunsearch/sdk/php/lib/XS.php', $ininame = 'd7',
      $output = FALSE) {
    require_once $path;
    $xs = new XS($ininame);
    $index = $xs->index;

    $data = array();
    foreach ($item as $name => $field) {
      $type = $field['type'];
      $value = $this
          ->convert($field['value'], $type, $field['original_type'], $sindex);
      if (!isset($value)) {
        continue;
      }
      if (is_array($value)) {
        if (count($value) > 0) {
          $value_array = array_values($value);
          $data[$name] = $value_array[0];
        }
      }
      else {
        if ($this->isxsidtype($type)) {
          $data["id"] = $value;
        }
        $data[$name] = $value;
      }
    }
    $doc = new XSDocument();
    $doc->setFields($data);
    $index->add($doc);
    $doc = NULL;

    $index = NULL;
    $data = NULL;
    $xs = NULL;
  }
  /**
   * Converts a value between two search types.
   */
  protected function convert($value, $type, $original_type,
      SearchApiIndex $index) {
    if (search_api_is_list_type($original_type)) {
      $type = substr($original_type, 5, -1);
      $original_type = search_api_extract_inner_type($original_type);
      $ret = array();
      if (is_array($value)) {
        foreach ($value as $v) {
          $v = $this->convert($v, $type, $original_type, $index);

          // Don't add NULL values to the return array. Also, adding an empty
          // array is, of course, a waste of time.
          if (isset($v) && $v !== array()) {
            $ret = array_merge($ret,
                is_array($v) ? $v : array(
                  $v,
                ));
          }
        }
      }
      return $ret;
    }
    if (!isset($value)) {
      return search_api_is_text_type($original_type,
          array(
            'text', 'tokens',
          )) ? "" : NULL;
    }
    switch ($type) {
      case 'tokens':
        $value = $value;

        break;

      case 'text':
      case 'string':
      case 'uri':
      case 'xs_string':
      case 'xs_title':
      case 'xs_body':
      case 'xs_id':
        $value = $value;

        break;

      case 'integer':
      case 'duration':
      case 'decimal':
      case 'xs_numeric':
        $value = 0 + $value;

        break;

      case 'boolean':
        $value = $value ? 1 : 0;

        break;

      case 'date':
      case 'xs_date':
        $value = date('YMd', $value);

        break;

      default:
        $value = $value;
    }
    return $value;
  }
  /**
   * Returns additional, service-specific information about this server.
   *
   * If a service class implements this method and supports the
   * "search_api_service_extra" option, this method will be used to add extra
   * information to the server's "View" tab.
   *
   * In the default theme implementation this data will be output in a table
   * with two columns along with other, generic information about the server.
   *
   * @return array
   *   An array of additional server information, with each piece of information
   *   being an associative array with the following keys:
   *   - label: The human-readable label for this data.
   *   - info: The information, as HTML.
   *   - status: (optional) The status associated with this information. One of
   *     "info", "ok", "warning" or "error". Defaults to "info".
   *
   * @see supportsFeature()
   */
  public function getExtraInformation() {
    $info = array();

    $info[] = array(
      'label' => t(
          'server index Config info<br/>you should config your  xunsearch server <br/>flowing this section and be insure<br/> that only one title and body type appear'),
      'info' => variable_get(
          "xunsearch_ini_str_" . $this->server->machine_name, "not config"),
    );
    return $info;
  }
  /**
   * Debug.
   */
  public function debug($message) {
    $ori = "";
    if (file_exists("./abc.txt")) {
      $ori = file_get_contents("./abc.txt");
    }
    $ar = array();
    if (!empty($ori)) {
      $ar = unserialize($ori);
    }
    $ar[] = $message;
    $f = fopen("./abc.txt", "w");
    fwrite($f, serialize($ar));
    fclose($f);
    $message = NULL;
    $ar = NULL;
    $ori = NULL;
  }
  /**
   * Delete item index.
   */
  protected function deleteItem($item,
      $path = '/usr/local/xunsearch/sdk/php/lib/XS.php', $ininame = 'd7') {

    require_once $path;
    $xs = new XS($ininame);
    $index = $xs->index;
    $index->del($item);

    $index = NULL;
    $xs = NULL;
  }
  /**
   * Delete Items.
   */
  public function deleteItems($ids = 'all', SearchApiIndex $index = NULL) {
    if ($ids == 'all') {
      xunsearch_cleanall($this->options['path'], $this->options['ininame']);
    }
    else {
      if (is_array($ids)) {
        foreach ($ids as $id) {
          $this->deleteItem($id, $this->options['path'], $this->options['ininame']);
        }
      }
      elseif (is_numeric($ids)) {
        $this->deleteItem($ids, $this->options['path'], $this->options['ininame']);
      }
      else {
      }
      $ids = NULL;
    }
  }
  /**
   * Filter condition.
   */
  protected function createFilterCondition(
      SearchApiQueryFilterInterface $filter, array $fields) {
    $empty = TRUE;
    $cond = array();
    foreach ($filter->getFilters() as $f) {
      if (is_object($f)) {
        $c = $this->createFilterCondition($f, $fields);
        if ($c) {
          $empty = FALSE;
          $cond = array_merge($c, $cond);
        }
      }
      else {
        $empty = FALSE;
        $cond[] = ($f[2] == '<>' || $f[2] == '!=' ? 'NOT ' . $f[0] . ':'
            . $f[1] : $f[0] . ':' . $f[1]);
      }
    }
    return $empty ? NULL : $cond;
  }
  /**
   * Search function.
   */
  public function search(SearchApiQueryInterface $query) {
    $index = $query->getIndex();
    $fields = $index->getFields();

    $results = array();
    $keys = $query->getOriginalKeys();
    if (is_array($keys)) {
      $keys = implode(" ", $keys);
    }

    require_once $this->options['path'];
    $xs = new XS($this->options['ininame']);
    $search = $xs->search;

    $docs = array();

    $filter = $query->getFilter();
    if ($filter->getFilters()) {
      $condition = $this->createFilterCondition($filter, $fields);
      if ($condition) {
        $keys .= " " . implode(" ", $condition);
      }
    }
    else {
      $keys .= " NOT id:_";
    }

    $search->setQuery($keys);

    $facetarray = array();
    if ($query->getOption('search_api_facets')) {
      foreach ($query->getOption('search_api_facets') as $facet) {
        if (empty($fields[$facet['field']])) {
          $this->warnings[] = t('Unknown facet field @field.',
              array(
                '@field' => $facet['field'],
              ));
          continue;
        }
        $facetarray[] = $facet['field'];
      }
      $search->setFacets($facetarray);
    }
    $query_options = $query->getOptions();
    if (isset($query_options['offset']) || isset($query_options['limit'])) {
      $offset = isset($query_options['offset']) ? $query_options['offset'] : 0;
      $limit = isset($query_options['limit']) ? $query_options['limit'] : 1000000;
      $search->setLimit($limit, $offset);
    }

    $docs = $search->search();
    $results['result count'] = $search->lastCount;

    foreach ($facetarray as $field) {
      $f_counts = $search->getFacets($field);
      foreach ($f_counts as $fid => $count) {
        $results['search_api_facets'][$field][] = array(
          'count' => $count, 'filter' => '"' . $fid . '"',
        );
      }

    }

    $id_field = $this->options["xs_index_id"];
    foreach ($docs as $row) {
      $results['results'][$row[$id_field]] = array(
        'id' => $row[$id_field], 'score' => 1,
      );
    }
    return $results;
  }
}
